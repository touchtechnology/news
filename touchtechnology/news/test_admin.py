from os.path import dirname, join

from django.core.urlresolvers import reverse
from django.test import TransactionTestCase
from django.test.utils import override_settings
from django.utils import timezone

TEST_DIR = join(dirname(__file__), 'tests')


class PublishedTimeZoneTestCase(TransactionTestCase):

    fixtures = ['test_data']
    urls = 'touchtechnology.news.test_urls'

    def test_sanity_check(self):
        self.assertTrue(True)
        self.assertFalse(False)

    def check_html_fragment(self, response, filename):
        html = open(join(TEST_DIR, filename)).read()
        self.assertContains(response, html, html=True)

    @override_settings(TIME_ZONE='UTC')
    def test_published_datetime_html(self):
        url = reverse('admin:news:article:edit', args=(1,))
        self.client.login(username='admin@example.com', password='password')
        response = self.client.get(url)
        for i in xrange(5):
            filename = 'select_date_time_published_%d.html' % i
            self.check_html_fragment(response, filename)

    def test_published_datetime_html_tz(self):
        timezone.activate('Australia/Sydney')
        url = reverse('admin:news:article:edit', args=(1,))
        self.client.login(username='admin@example.com', password='password')
        response = self.client.get(url)
        filename = 'select_date_time_published_3-Sydney.html'
        self.check_html_fragment(response, filename)
