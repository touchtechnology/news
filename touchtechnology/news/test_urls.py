from django.conf.urls import patterns, include, url
from django.contrib.admin import autodiscover

from touchtechnology.admin import sites as admin
from touchtechnology.news.sites import NewsSite
autodiscover()

news = NewsSite()

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^news/', include(news.urls)),
)
