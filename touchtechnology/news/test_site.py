from test_plus import TestCase


class InvalidDateTest(TestCase):

    urls = 'touchtechnology.news.test_urls'

    def test_archive_day(self):
        self.get('news:article', year='2013', month='feb', day='31')
        self.response_404()

    def test_article(self):
        self.get('news:article', year='2013', month='feb', day='31',
                 slug='tfms-new-generaltechnical-manager')
        self.response_404()


class FeedTest(TestCase):

    urls = 'touchtechnology.news.test_urls'

    def test_atom(self):
        self.assertGoodView('news:feed', format='atom')

    def test_rss(self):
        self.assertGoodView('news:feed', format='rss')
