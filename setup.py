import glob
import os
import touchtechnology.news
from setuptools import setup, find_packages

version = touchtechnology.news.__version__
author = 'Touch Technology Pty Ltd'
author_email = 'support@touchtechnology.com.au'
url = 'https://bitbucket.org/touchtechnology/news/'

if os.environ.get('CI'):
    branch = os.environ.get('CI_BRANCH')
    if branch != 'master':
        suffix = ".dev" + os.environ.get('CI_BUILD_NUMBER')
        if not version.endswith(suffix):
            version += suffix
            with open(glob.glob('*/*/VERSION.txt')[0], 'w') as txt:
                txt.write(version)
        author = os.environ.get('CI_COMMITTER_NAME', author)
        author_email = os.environ.get('CI_COMMITTER_EMAIL', author_email)
        url = os.environ.get('CI_BUILD_URL', url)

setup(
    name='touchtechnology-news',
    version=version,
    author=author,
    author_email=author_email,
    url=url,
    description='News article library.',
    packages=find_packages(exclude=["test_app"]),
    install_requires=[
        'touchtechnology-content>=3.0.2',
        'django-imagekit',
    ],
    include_package_data=True,
    namespace_packages=['touchtechnology'],
    zip_safe=False,
)
