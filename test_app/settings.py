from os.path import dirname, join as basejoin, realpath

join = lambda *args: realpath(basejoin(*args))

PROJECT_DIR = join(dirname(__file__), '..')

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
    }
}

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.CryptPasswordHasher',
)

TIME_ZONE = 'Australia/Sydney'
LANGUAGE_CODE = 'en-AU'

USE_TZ = True

MEDIA_ROOT = join(PROJECT_DIR, 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = join(PROJECT_DIR, 'static')
STATIC_URL = '/static/'

SECRET_KEY = '2bksb4rhbv7i1$!5xzux0&amp;&amp;sl2@de@k6sxb4(zlj4l)+xds#2i'
SITE_ID = 1

ROOT_URLCONF = 'test_app.urls'
WSGI_APPLICATION = 'test_app.wsgi.application'

INSTALLED_APPS = (
    'mptt',
    'guardian',
    'bootstrap3',
    'django_gravatar',

    'touchtechnology.common',
    'touchtechnology.admin',
    'touchtechnology.content',
    'touchtechnology.news',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',
    'django.contrib.sessions',
)

AUTHENTICATION_BACKENDS = (
    'touchtechnology.common.backends.auth.EmailUserSubclassBackend',
    'guardian.backends.ObjectPermissionBackend',
)

ANONYMOUS_USER_ID = -1
ANONYMOUS_DEFAULT_USERNAME_VALUE = 'anonymous'

FIXTURE_DIRS = (
    join(PROJECT_DIR, 'fixtures'),
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'touchtechnology.common.middleware.TimezoneMiddleware',
    'touchtechnology.content.middleware.SitemapNodeMiddleware',
)
